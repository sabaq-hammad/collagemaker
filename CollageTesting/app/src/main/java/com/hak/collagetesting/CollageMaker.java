package com.hak.collagetesting;

import android.graphics.Canvas;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.jcmore2.collage.CollageView;
import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import android.graphics.BitmapFactory;
import android.widget.Toast;
import java.util.Random;
public class CollageMaker extends AppCompatActivity {
    RelativeLayout layoutCollage;
    Button btnClearCollage;
    Button btnUpload;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_maker);
        selectImagesFromAlbums();
        initViews();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            ArrayList<Bitmap> imagesBitmaps = new ArrayList<>();
            btnUpload.setVisibility(View.INVISIBLE);
            for (int i = 0; i < images.size(); i++) {
                imagesBitmaps.add(decodeSampledBitmapFromResource(images.get(i).path,280,280));
                Log.d("abc", "onActivityResult: "+images.get(i).path);

            }
            createNewCollage(imagesBitmaps);

        }
    }

    void initViews() {
        btnUpload = (Button) findViewById(R.id.btnUpload);
        btnUpload.setVisibility(View.INVISIBLE);
        btnClearCollage = (Button) findViewById(R.id.btnClear);
        btnSave = (Button) findViewById(R.id.btnSave);
        layoutCollage = (RelativeLayout) findViewById(R.id.collage);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveImage(layoutCollage);
            }
        });
        btnClearCollage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearCollage();
            }
        });
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImagesFromAlbums();
            }
        });

    }

    public void createNewCollage(ArrayList<Bitmap> images) {
        layoutCollage.removeAllViews();
        CollageView collage = new CollageView(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(20, 20, 20, 20);
        collage.setLayoutParams(params);
        collage.setFixedCollage(false);
        collage.createCollageBitmaps(images);
        collage.setBackgroundColor(Color.WHITE);
        layoutCollage.setBackgroundColor(Color.WHITE);
        layoutCollage.addView(collage);

    }
    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight) {
        Log.d("path", path);
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public void clearCollage() {
        layoutCollage.removeAllViews();
        btnUpload.setVisibility(View.VISIBLE);
    }
    public void toggleButtonVisibility(Button btn,Boolean b){
      if(b==true)
          btn.setVisibility(View.VISIBLE);
        else
            btn.setVisibility(View.INVISIBLE);
    }

    private void selectImagesFromAlbums() {
        Intent intent = new Intent(this, AlbumSelectActivity.class);
//set limit on number of images that can be selected, default is 10
//        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 10);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }
    void SaveImage(View v) {
        Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        v.draw(c);
        try {
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = "Image-" + n + ".png";
            File outputFile = new File(Environment.getExternalStorageDirectory()+File.separator+"/NeoCollage/"+fname ); // Where to save it
            outputFile.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(outputFile);
            boolean success = bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            if(success)
                Toast.makeText(this, "Successfully saved", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "Error while saving", Toast.LENGTH_SHORT).show();
            out.close();
        }catch (Exception e){
            Toast.makeText(CollageMaker.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

}
